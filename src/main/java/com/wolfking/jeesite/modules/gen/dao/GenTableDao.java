/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.wolfking.jeesite.modules.gen.dao;

import com.wolfking.jeesite.common.persistence.CrudDao;
import com.wolfking.jeesite.modules.gen.entity.GenTable;
import org.apache.ibatis.annotations.Mapper;

/**
 * 业务表DAO接口
 * @author ThinkGem
 * @version 2013-10-15
 */
@Mapper
public interface GenTableDao extends CrudDao<GenTable> {
	
}
