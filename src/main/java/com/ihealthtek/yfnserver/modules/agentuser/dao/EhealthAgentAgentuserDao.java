/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.ihealthtek.yfnserver.modules.agentuser.dao;

import com.wolfking.jeesite.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.ihealthtek.yfnserver.modules.agentuser.entity.EhealthAgentAgentuser;

/**
 * 代理商表DAO接口
 * @author charlie
 * @version 2018-04-12
 */
@Mapper
public interface EhealthAgentAgentuserDao extends CrudDao<EhealthAgentAgentuser> {
	
}