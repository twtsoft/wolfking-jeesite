/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.ihealthtek.yfnserver.modules.agentuser.entity;

import org.hibernate.validator.constraints.Length;

import com.wolfking.jeesite.common.persistence.DataEntity;

/**
 * 代理商表Entity
 * @author charlie
 * @version 2018-04-12
 */
public class EhealthAgentAgentuser extends DataEntity<EhealthAgentAgentuser> {
	
	private static final long serialVersionUID = 1L;
	private String sysUserName;		// 用户名
	private String headImg;		// 头像
	private String sex;		// 性别
	private String identityId;		// 身份证
	private String agentType;		// 类别
	private String agentLevel;		// 级别
	private String pId;		// 上级编号
	private String belongAreaId;		// 所在区域
	private String contactAddress;		// 联系地址
	private String createTime;		// 创建时间
	private String company;		// 公司
	private String department;		// 部门
	private String name;		// 姓名
	private String password;		// 密码
	private String roleId;		// 角色
	private String phone;		// 电话
	private String email;		// 邮箱
	private String managerAreaId;		// 地区
	private String unionid;		// unionid
	
	public EhealthAgentAgentuser() {
		super();
	}

	public EhealthAgentAgentuser(String id){
		super(id);
	}

	@Length(min=0, max=255, message="用户名长度必须介于 0 和 255 之间")
	public String getSysUserName() {
		return sysUserName;
	}

	public void setSysUserName(String sysUserName) {
		this.sysUserName = sysUserName;
	}
	
	@Length(min=0, max=255, message="头像长度必须介于 0 和 255 之间")
	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	
	@Length(min=0, max=255, message="性别长度必须介于 0 和 255 之间")
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@Length(min=0, max=255, message="身份证长度必须介于 0 和 255 之间")
	public String getIdentityId() {
		return identityId;
	}

	public void setIdentityId(String identityId) {
		this.identityId = identityId;
	}
	
	@Length(min=0, max=255, message="类别长度必须介于 0 和 255 之间")
	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}
	
	@Length(min=0, max=255, message="级别长度必须介于 0 和 255 之间")
	public String getAgentLevel() {
		return agentLevel;
	}

	public void setAgentLevel(String agentLevel) {
		this.agentLevel = agentLevel;
	}
	
	@Length(min=1, max=255, message="上级编号长度必须介于 1 和 255 之间")
	public String getPId() {
		return pId;
	}

	public void setPId(String pId) {
		this.pId = pId;
	}
	
	@Length(min=0, max=255, message="所在区域长度必须介于 0 和 255 之间")
	public String getBelongAreaId() {
		return belongAreaId;
	}

	public void setBelongAreaId(String belongAreaId) {
		this.belongAreaId = belongAreaId;
	}
	
	@Length(min=0, max=255, message="联系地址长度必须介于 0 和 255 之间")
	public String getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	
	@Length(min=0, max=255, message="创建时间长度必须介于 0 和 255 之间")
	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	@Length(min=0, max=30, message="公司长度必须介于 0 和 30 之间")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@Length(min=0, max=30, message="部门长度必须介于 0 和 30 之间")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Length(min=0, max=255, message="姓名长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="密码长度必须介于 0 和 255 之间")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Length(min=0, max=100, message="角色长度必须介于 0 和 100 之间")
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	@Length(min=0, max=32, message="电话长度必须介于 0 和 32 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=32, message="邮箱长度必须介于 0 和 32 之间")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Length(min=0, max=255, message="地区长度必须介于 0 和 255 之间")
	public String getManagerAreaId() {
		return managerAreaId;
	}

	public void setManagerAreaId(String managerAreaId) {
		this.managerAreaId = managerAreaId;
	}
	
	@Length(min=0, max=255, message="unionid长度必须介于 0 和 255 之间")
	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	
}