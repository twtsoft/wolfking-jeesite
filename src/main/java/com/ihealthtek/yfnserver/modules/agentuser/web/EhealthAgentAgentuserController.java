/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.ihealthtek.yfnserver.modules.agentuser.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wolfking.jeesite.common.config.Global;
import com.wolfking.jeesite.common.persistence.Page;
import com.wolfking.jeesite.common.web.BaseController;
import com.wolfking.jeesite.common.utils.StringUtils;
import com.ihealthtek.yfnserver.modules.agentuser.entity.EhealthAgentAgentuser;
import com.ihealthtek.yfnserver.modules.agentuser.service.EhealthAgentAgentuserService;

/**
 * 代理商表Controller
 * @author charlie
 * @version 2018-04-12
 */
@Controller
@RequestMapping(value = "${adminPath}/agentuser/ehealthAgentAgentuser")
public class EhealthAgentAgentuserController extends BaseController {

	@Autowired
	private EhealthAgentAgentuserService ehealthAgentAgentuserService;
	
	@ModelAttribute
	public EhealthAgentAgentuser get(@RequestParam(required=false) String id) {
		EhealthAgentAgentuser entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = ehealthAgentAgentuserService.get(id);
		}
		if (entity == null){
			entity = new EhealthAgentAgentuser();
		}
		return entity;
	}
	
	@RequiresPermissions("agentuser:ehealthAgentAgentuser:view")
	@RequestMapping(value = {"list", ""})
	public String list(EhealthAgentAgentuser ehealthAgentAgentuser, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<EhealthAgentAgentuser> page = ehealthAgentAgentuserService.findPage(new Page<EhealthAgentAgentuser>(request, response), ehealthAgentAgentuser); 
		model.addAttribute("page", page);
		return "modules/agentuser/ehealthAgentAgentuserList";
	}

	@RequiresPermissions("agentuser:ehealthAgentAgentuser:view")
	@RequestMapping(value = "form")
	public String form(EhealthAgentAgentuser ehealthAgentAgentuser, Model model) {
		model.addAttribute("ehealthAgentAgentuser", ehealthAgentAgentuser);
		return "modules/agentuser/ehealthAgentAgentuserForm";
	}

	@RequiresPermissions("agentuser:ehealthAgentAgentuser:edit")
	@RequestMapping(value = "save")
	public String save(EhealthAgentAgentuser ehealthAgentAgentuser, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, ehealthAgentAgentuser)){
			return form(ehealthAgentAgentuser, model);
		}
		ehealthAgentAgentuserService.save(ehealthAgentAgentuser);
		addMessage(redirectAttributes, "保存代理商成功");
		return "redirect:"+Global.getAdminPath()+"/agentuser/ehealthAgentAgentuser/?repage";
	}
	
	@RequiresPermissions("agentuser:ehealthAgentAgentuser:edit")
	@RequestMapping(value = "delete")
	public String delete(EhealthAgentAgentuser ehealthAgentAgentuser, RedirectAttributes redirectAttributes) {
		ehealthAgentAgentuserService.delete(ehealthAgentAgentuser);
		addMessage(redirectAttributes, "删除代理商成功");
		return "redirect:"+Global.getAdminPath()+"/agentuser/ehealthAgentAgentuser/?repage";
	}

}