/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.ihealthtek.yfnserver.modules.agentuser.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolfking.jeesite.common.persistence.Page;
import com.wolfking.jeesite.common.service.CrudService;
import com.ihealthtek.yfnserver.modules.agentuser.entity.EhealthAgentAgentuser;
import com.ihealthtek.yfnserver.modules.agentuser.dao.EhealthAgentAgentuserDao;

/**
 * 代理商表Service
 * @author charlie
 * @version 2018-04-12
 */
@Service
@Transactional(readOnly = true)
public class EhealthAgentAgentuserService extends CrudService<EhealthAgentAgentuserDao, EhealthAgentAgentuser> {

	public EhealthAgentAgentuser get(String id) {
		return super.get(id);
	}
	
	public List<EhealthAgentAgentuser> findList(EhealthAgentAgentuser ehealthAgentAgentuser) {
		return super.findList(ehealthAgentAgentuser);
	}
	
	public Page<EhealthAgentAgentuser> findPage(Page<EhealthAgentAgentuser> page, EhealthAgentAgentuser ehealthAgentAgentuser) {
		return super.findPage(page, ehealthAgentAgentuser);
	}
	
	@Transactional(readOnly = false)
	public void save(EhealthAgentAgentuser ehealthAgentAgentuser) {
		super.save(ehealthAgentAgentuser);
	}
	
	@Transactional(readOnly = false)
	public void delete(EhealthAgentAgentuser ehealthAgentAgentuser) {
		super.delete(ehealthAgentAgentuser);
	}
	
}