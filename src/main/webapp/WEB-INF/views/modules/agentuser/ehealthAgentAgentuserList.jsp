<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/include/head.jsp" %>
	<title>代理商管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/agentuser/ehealthAgentAgentuser/">代理商列表</a></li>
		<shiro:hasPermission name="agentuser:ehealthAgentAgentuser:edit"><li><a href="${ctx}/agentuser/ehealthAgentAgentuser/form">代理商添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="ehealthAgentAgentuser" action="${ctx}/agentuser/ehealthAgentAgentuser/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>用户名：</label>
				<form:input path="sysUserName" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>姓名：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>用户名</th>
				<th>头像</th>
				<th>性别</th>
				<th>姓名</th>
				<shiro:hasPermission name="agentuser:ehealthAgentAgentuser:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="ehealthAgentAgentuser">
			<tr>
				<td><a href="${ctx}/agentuser/ehealthAgentAgentuser/form?id=${ehealthAgentAgentuser.id}">
					${ehealthAgentAgentuser.sysUserName}
				</a></td>
				<td>
					${ehealthAgentAgentuser.headImg}
				</td>
				<td>
					${ehealthAgentAgentuser.sex}
				</td>
				<td>
					${ehealthAgentAgentuser.name}
				</td>
				<shiro:hasPermission name="agentuser:ehealthAgentAgentuser:edit"><td>
    				<a href="${ctx}/agentuser/ehealthAgentAgentuser/form?id=${ehealthAgentAgentuser.id}">修改</a>
					<a href="${ctx}/agentuser/ehealthAgentAgentuser/delete?id=${ehealthAgentAgentuser.id}" onclick="return confirmx('确认要删除该代理商吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>