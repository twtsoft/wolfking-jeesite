/*
Navicat MySQL Data Transfer

Source Server         : 139.129.201.108测试新
Source Server Version : 50547
Source Host           : 139.129.201.108:3320
Source Database       : yfnserver

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2018-04-04 15:04:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ehealth_agent_agentuser
-- ----------------------------
DROP TABLE IF EXISTS `ehealth_agent_agentuser`;
CREATE TABLE `ehealth_agent_agentuser` (
  `ID` varchar(255) NOT NULL,
  `sys_user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `head_img` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` varchar(255) DEFAULT NULL COMMENT '性别',
  `identity_id` varchar(255) DEFAULT NULL COMMENT '身份证',
  `agent_type` varchar(255) DEFAULT NULL COMMENT '类别（管理、销售/经销商/分销商、装机人员）',
  `agent_level` varchar(255) DEFAULT NULL COMMENT '级别(总裁、销售总经理、大区经理、销售经理、……)',
  `p_id` varchar(255) NOT NULL COMMENT '上级编号',
  `belong_area_id` varchar(255) DEFAULT NULL COMMENT '所在区域',
  `contact_address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `company` varchar(30) DEFAULT NULL,
  `department` varchar(30) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `manager_area_id` varchar(255) DEFAULT NULL,
  `unionid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ehealth_agent_agentuser
-- ----------------------------
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482988246647569123', 'tangshiyan', 'agentUser/1482988246647569123?t=1498714975416', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:42:55', '', '', '唐世燕', '', '', '13813396387', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482988711334509284', 'sunwenwen', 'agentUser/1482988711334509284?t=1498715030801', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:43:50', '', '', '孙雯雯', '', '', '15951701242', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482988898467576549', 'liweiwei', 'agentUser/1482988898467576549?t=1498715053116', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:44:12', '', '', '李卫卫', '', '', '15951979690', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482989171399326438', 'zhangquan', 'agentUser/1482989171399326438?t=1498715085626', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:44:45', '', '', '张权', '', '', '15851891343', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482989440581368551', 'zhaoqin', 'agentUser/1482989440581368551?t=1498715117728', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:45:17', '', '', '赵琴', '', '', '15251742746', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482989605266520808', 'yunhong', 'agentUser/1482989605266520808?t=1498715137352', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:45:37', '', '', '云红', '', '', '18602505604', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482989834728504041', 'wuxuelan', 'agentUser/1482989834728504041?t=1498715164716', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:46:04', '', '', '吴学兰', '', '', '15951640093', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482990006963403498', 'gaoguohong', 'agentUser/1482990006963403498?t=1498715185262', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:46:25', '', '', '高国红', '', '', '15951846790', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482990237952113387', 'wuyuewen', 'agentUser/1482990237952113387?t=1498715212795', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:46:52', '', '', '吴月雯', '', '', '15996473138', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482990431376636652', 'chenjiyu', 'agentUser/1482990431376636652?t=1498715235845', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:47:15', '', '', '陈继玉', '', '', '15851883365', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482990590709857005', 'lixiang', 'agentUser/1482990590709857005?t=1498715254851', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:47:34', '', '', '李香', '', '', '15715173109', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482990797388380910', 'zenghaiyan', 'agentUser/1482990797388380910?t=1498715279479', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:47:59', '', '', '曾海燕', '', '', '15850727936', '', '', '');
INSERT INTO `ehealth_agent_agentuser` VALUES ('1482990962123864815', 'lizhengli', 'agentUser/1482990962123864815?t=1498715299098', '女', '', 'role_71', '', '0', '', '', '2017-06-29 13:48:19', '', '', '李正丽', '', '', '18761809089', '', '', '');
